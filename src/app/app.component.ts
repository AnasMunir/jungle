import { Component,ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
   @ViewChild(Nav) nav: Nav;
  rootPage:any = 'Home'; 

  constructor(platform: Platform, public menuCtrl: MenuController, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

 

  }
   openPhoto(){
    this.menuCtrl.close();
    this.nav.push('Photo');
  }
    openAbout(){
    this.menuCtrl.close();
    this.nav.push('About');
  }
    openTC(){
    this.menuCtrl.close();
    this.nav.push('TermCondition');
  }
}

