import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';
/*
  Generated class for the Server provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Server {

  constructor(private http: Http) {
    console.log('Hello Server Provider');
  }

  checkStatus(){
     return this.http.get('http://51.254.56.71/movieapp/index.php/data/status')
        .do(this.logResponse)
   .map(this.extractData)
   .catch(this.catchError)
  }

  getContent(){
   return this.http.get('http://51.254.56.71/movieapp/index.php/data/photos')
   .do(this.logResponse)
   .map(this.extractData)
   .catch(this.catchError)
  }

  private catchError(error: Response | any){
    console.log(error);
    return Observable.throw(error.json().error || "Server error");
    
  }

  private logResponse(res: Response){
    console.log(res);
  }
  private extractData(res: Response){
    return res.json();
  }
}
