import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TermCondition } from './term-condition';

@NgModule({
  declarations: [
    TermCondition,
  ],
  imports: [
    IonicPageModule.forChild(TermCondition),
  ],
  exports: [
    TermCondition
  ]
})
export class TermConditionModule {}
